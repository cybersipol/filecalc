﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using NSubstitute;
using FileCalc.IO.Interfaces;
using System.IO;
using FileCalc.Tests.Unit.IO;
using FileCalc.IO;
using FileCalc.Calc;

namespace FileCalc.Tests.Unit
{
    [TestFixture]
    public class FileCalcClient_Tests
    {
        private FileCalcClient sut = null;
        private ILastLineReader mockLastLineReader = null;
        private TextReader mockTextReader = null;
        private Stream contentStream = null;

        [SetUp]
        public void Init()
        {
        }

        [Test]
        public void CalcFile_NoLastLineReader_ThrowExcpetion()
        {
            CreateSUT(String.Empty);
            Exception ex = Assert.Throws<ArgumentNullException>(() => { sut.CalcFile();  });
            StringAssert.Contains("No installed LastLineReader", ex.Message);
        }

        [Test]
        public void CalcFile_EmptyContent_ThrowException()
        {
            CreateSUT(String.Empty);

            sut.LastLineReader = CreateMockLastLineReader();
            sut.LastLineReader.GetLastLine().Returns(String.Empty);

            Decimal result = -1;
            Assert.Throws<Exception>(() =>
            {
                result = sut.CalcFile();
            });
        }

        [Test]
        public void CalcFile_NoCalcLineFactory_ThrowException()
        {
            CreateSUT(String.Empty);

            sut.LastLineReader = CreateMockLastLineReader();
            sut.LastLineReader.GetLastLine().Returns(String.Empty);

            sut.CalcLineFactory = null;

            Decimal result = -1;
            Exception ex = Assert.Throws<ArgumentNullException>(() =>
            {
                result = sut.CalcFile();
            });

            StringAssert.Contains("No installed CalcLineFactory", ex.Message);
        }

        [Test]
        public void CalcFile_NoApplyLine_ThrowException()
        {
            CreateSUT("aaa"+Environment.NewLine+"bbb");

            sut.LastLineReader = CreateMockLastLineReader();
            sut.LastLineReader.GetLastLine().Returns("bbb");

            Decimal result = -1;
            Exception ex = Assert.Throws<Exception>(() =>
            {
                result = sut.CalcFile();
            });

            StringAssert.Contains("Wrong command", ex.Message);
        }

        [Test]
        public void CalcFile_NoApplyLineButOtherCommandAreOk_ThrowException()
        {
            CreateSUT("multiply 2\r\n\r\nsubtract 1\r\n");

            sut.LastLineReader = CreateMockLastLineReader();
            sut.LastLineReader.GetLastLine().Returns("subtract 1");

            Decimal result = -1;
            Exception ex = Assert.Throws<Exception>(() =>
            {
                result = sut.CalcFile();
            });

            StringAssert.Contains("Cannot localize apply line", ex.Message);
        }


        [Test]
        public void CalcFile_OlnyApplyLine_ReturnApplyValue()
        {
            Decimal expectedValue = 10;
            string lines = "apply "+expectedValue.ToString();
            CreateSUT(lines);

            sut.LastLineReader = CreateMockLastLineReader();
            sut.LastLineReader.GetLastLine().Returns(lines);

            Decimal result = sut.CalcFile();
            Assert.AreEqual(expectedValue, result);
        }

        [TestCase("apply 5",5)]
        [TestCase("add 2\r\napply 5", 7)]
        [TestCase("multiply 2\r\n\r\nsubtract 1\r\n\r\napply 10\r\n\r\n", 19)]
        public void CalcFile_CorrectContent_ReturnOkValue(string content, Decimal expectedValue)
        {
            CreateSUT(content);

            sut.LastLineReader = new LastNoEmptyLineReader(contentStream);

            Decimal result = sut.CalcFile();
            Assert.AreEqual(expectedValue, result);
        }

        [TestCase("apply 5",5)]
        [TestCase("add 2\r\napply 5", 7)]
        [TestCase("multiply 2\r\n\r\nsubtract 1\r\n\r\napply 10\r\n\r\n", 19)]
        public void CalcFile_SeekStreamToEndBefore_ReturnOkValue(string content, Decimal expectedValue)
        {
            CreateSUT(content);
            contentStream.Seek(0, SeekOrigin.End);

            sut.LastLineReader = new LastNoEmptyLineReader(contentStream);

            Decimal result = sut.CalcFile();
            Assert.AreEqual(expectedValue, result);
        }

        private FileCalcClient CreateSUT(Stream stream, Encoding encoding)
        {
            contentStream = stream;
            sut = new FileCalcClient(stream, encoding);
            AddToSUTDefaultCalcLineFactory();
            return sut;
        }

        private FileCalcClient CreateSUT(Stream stream)
        {
            return CreateSUT(stream, Encoding.UTF8);
        }

        private FileCalcClient CreateSUT(string fileContent, Encoding encoding)
        {
            return CreateSUT(CreateStreamFromString(fileContent, encoding), encoding);
        }

        private FileCalcClient CreateSUT(string fileContent)
        {
            return CreateSUT(fileContent, Encoding.UTF8);
        }

        private void AddToSUTDefaultCalcLineFactory()
        {
            if (sut == null) return;
            sut.CalcLineFactory = new CalcLineFactory();
        }

        private Stream CreateStreamFromString(string s, Encoding encoding)
        {
            return LastNoEmptyLineReader_Tests.CreateStreamFromString(s, encoding);
        }

        private Stream CreateStreamFromString(string s)
        {
            return CreateStreamFromString(s, Encoding.UTF8);
        }

        private ILastLineReader CreateMockLastLineReader()
        {
            mockLastLineReader = Substitute.For<ILastLineReader>();
            return mockLastLineReader;
        }
    }
}
