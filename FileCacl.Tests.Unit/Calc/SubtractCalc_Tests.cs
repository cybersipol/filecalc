﻿using FileCalc.Calc;
using NUnit.Framework;

namespace FileCalc.Tests.Unit
{
    [TestFixture]
    public class SubtractCalc_Tests
    {
        private SubtractCalc sut = null;

        [Test]
        public void Calculate_byDefault_ReturnOkNumber()
        {
            sut = new SubtractCalc(new ApplyNumberCalc(5), 3);

            Assert.AreEqual(2, sut.Calculate());
        }

        [Test]
        public void Calculate_MinusParam_ReturnOkNumber()
        {
            sut = new SubtractCalc(new ApplyNumberCalc(5), -3);

            Assert.AreEqual(8, sut.Calculate());
        }
    }
}
