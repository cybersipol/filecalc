﻿using FileCalc.Calc;
using NUnit.Framework;

namespace FileCalc.Tests.Unit
{

    [TestFixture]
    public class AddCalc_Tests
    {
        private AddCalc sut = null;

        [Test]
        public void Calculate_byDefault_ReturnOkNumber()
        {
            sut = new AddCalc(new ApplyNumberCalc(5), 3);

            Assert.AreEqual(8, sut.Calculate());
        }

        [Test]
        public void Calculate_MinusParam_ReturnOkNumber()
        {
            sut = new AddCalc(new ApplyNumberCalc(5), -3);

            Assert.AreEqual(2, sut.Calculate());
        }
    }
}
