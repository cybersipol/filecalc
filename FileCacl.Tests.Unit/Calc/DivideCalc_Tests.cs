﻿using FileCalc.Calc;
using NUnit.Framework;
namespace FileCalc.Tests.Unit
{
    [TestFixture]
    public class DivideCalc_Tests
    {
        private DivideCalc sut = null;

        [Test]
        public void Calculate_byDefault_ReturnOkNumber()
        {
            sut = new DivideCalc(new ApplyNumberCalc(15), 3);

            Assert.AreEqual(5, sut.Calculate());
        }

    }
}
