﻿using FileCalc.Calc;
using NUnit.Framework;

namespace FileCalc.Tests.Unit
{
    [TestFixture]
    public class MultiplyCalc_Tests
    {
        private MultiplyCalc sut = null;

        [Test]
        public void Calculate_byDefault_ReturnOkNumber()
        {
            sut = new MultiplyCalc(new ApplyNumberCalc(5), 3);

            Assert.AreEqual(5*3, sut.Calculate());
        }

        [Test]
        public void Calculate_WithAddCalc_ReturnOkNumber()
        {
            sut = new MultiplyCalc(new AddCalc(new ApplyNumberCalc(5), 4), 3);

            Assert.AreEqual(((5+4)*3), sut.Calculate());
        }
    }
}
