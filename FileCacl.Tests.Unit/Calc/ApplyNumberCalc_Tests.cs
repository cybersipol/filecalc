﻿using FileCalc.Calc;
using NUnit.Framework;

namespace FileCalc.Tests.Unit
{
    [TestFixture]
    public class ApplyNumberCalc_Tests
    {
        private ApplyNumberCalc sut = null; 

        [SetUp]
        public void Init()
        {
            sut = new ApplyNumberCalc(0);
        }

        [Test]
        public void Calculate_byDefault_ReturnNumber()
        {
            int expceted = 5;
            sut = new ApplyNumberCalc(expceted);
            Assert.AreEqual(expceted, sut.Calculate());
        }
    }
}
