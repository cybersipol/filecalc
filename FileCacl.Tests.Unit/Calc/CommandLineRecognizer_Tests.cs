﻿using FileCalc.Calc;
using NUnit.Framework;
using System.Collections.Generic;
using System;
using FileCalc.Calc.Interfaces;

namespace FileCalc.Tests.Unit.Calc
{
    [TestFixture]
    public class CommandLineRecognizer_Tests
    {
        private CalcCommandLineRecognizer sut = null;
        private List<string> commands = null;

        [Test]
        public void LineRecognition_NullLine_ThrowException()
        {
            CreateSUTWithDefaultCommands();
            sut.Line = null;

            Assert.Throws<ArgumentNullException>(() =>
            {
                sut.LineRecognition();
            });

        }

        [Test]
        public void LineRecognition_EmptyLine_ThrowException()
        {
            CreateSUTWithDefaultCommands();
            sut.Line = String.Empty;

            Exception ex = Assert.Throws<Exception>(() =>
            {
                sut.LineRecognition();
            });

            StringAssert.Contains("Wrong command", ex.Message);
        }

        [Test]
        public void LineRecognition_WrongCommandLine_ThrowException()
        {
            CreateSUTWithDefaultCommands();
            sut.Line = commands[0] + "aaaa 16";

            Exception ex = Assert.Throws<Exception>(() =>
            {
                sut.LineRecognition();
            });

            StringAssert.Contains("Wrong command", ex.Message);
        }

        [TestCase("hoho 12")]
        [TestCase("end 18")]
        [TestCase("add 13,4")]
        [TestCase("apply 40")]
        [TestCase("apply bleblublo")]
        public void LineRecognition_OkCommand_ReturnILineCommand(string line)
        {
            CreateSUTWithDefaultCommands();
            sut.Line = line;

            object result = sut.LineRecognition();
            Assert.IsNotNull(result);
            Assert.IsInstanceOf<ILineCommand<ICalc>>(result);
        }

        #region Help functions

        private CalcCommandLineRecognizer CreateSUT(List<string> commands)
        {
            sut = new CalcCommandLineRecognizer(commands, null);
            return sut;
        }

        private CalcCommandLineRecognizer CreateSUTWithDefaultCommands()
        {
            commands = new List<string>();
            commands.Add("apply");
            commands.Add("modulo");
            commands.Add("hoho");
            commands.Add("add");
            commands.Add("end");
            return CreateSUT(commands);
        }

        #endregion

    }
}