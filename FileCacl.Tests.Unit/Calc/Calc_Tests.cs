﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FileCalc.Calc;
using FileCalc.Calc.Interfaces;
using FileCalc.IO;
using NUnit.Framework;
using FileCalc.Tests.Unit.IO;

namespace FileCalc.Tests.Unit.Calc
{
    [TestFixture]
    class Calc_Tests
    {
        
        [TestCase("apply 10", 10)]
        [TestCase("multiply 3\r\napply 10", 30)]
        [TestCase("multiply 2\r\nadd 1\r\napply 10", 21)]
        [TestCase("multiply 2\r\n\r\nadd 1\r\n\r\napply 10", 21)]
        [TestCase("multiply 2\r\n\r\nadd 1\r\n\r\napply 10\r\n", 21)]
        [TestCase("multiply 2\r\n\r\nadd 1\r\n\r\napply 10\r\n\r\n", 21)]
        [TestCase("multiply 2\r\n\r\nsubtract 1\r\n\r\napply 10\r\n\r\n", 19)]
        public void FullTest_ReturnOkNumber(string fileContents, Decimal expectedResult)
        {
            Console.WriteLine("content: [{0}]", fileContents);
            MemoryStream memoryStream = (MemoryStream)LastNoEmptyLineReader_Tests.CreateStreamFromString(fileContents);

            NoEmptyTrimLineReader textReader = new NoEmptyTrimLineReader(memoryStream);

            LastNoEmptyLineReader reader = new LastNoEmptyLineReader(memoryStream);
            string lastLine = reader.GetLastLine();

            CalcLineFactory calcFactory = new CalcLineFactory();
            //calcFactory.LineWriter = Console.Out;
            calcFactory.Line = lastLine;
            calcFactory.CurrentCalc = null;
            Console.WriteLine("CurrentCalcType: {0}, Param: {1}, Hash: {2} [START]", calcFactory.CurrentCalc?.GetType().ToString() ?? "(null)", calcFactory.CurrentCalc?.Param.ToString() ?? "(null)", calcFactory.CurrentCalc?.GetHashCode().ToString() ?? "(null)");

            calcFactory.CreateCalcFromLine();
            Console.WriteLine("CurrentCalcType: {0}, Param: {1}, Hash: {2} [LASTLINE]", calcFactory.CurrentCalc?.GetType().ToString() ?? "(null)", calcFactory.CurrentCalc?.Param.ToString() ?? "(null)", calcFactory.CurrentCalc?.GetHashCode().ToString() ?? "(null)");

            string line = textReader.ReadLine();
            while (line != null)
            {
                calcFactory.Line = line;

                calcFactory.CreateCalcFromLine();
                Console.WriteLine("CurrentCalcType: {0}, Param: {1}, Hash: {2}", calcFactory.CurrentCalc?.GetType().ToString() ?? "(null)", calcFactory.CurrentCalc?.Param.ToString() ?? "(null)", calcFactory.CurrentCalc?.GetHashCode().ToString() ?? "(null)");
                line = textReader.ReadLine();
            }

            Console.WriteLine("CurrentCalcType: {0}, Param: {1}, Hash: {2} [END]", calcFactory.CurrentCalc?.GetType().ToString() ?? "(null)", calcFactory.CurrentCalc?.Param.ToString() ?? "(null)", calcFactory.CurrentCalc?.GetHashCode().ToString() ?? "(null)");
            Decimal result = calcFactory.CurrentCalc?.Calculate() ?? -1;

            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("aksjsa 2\r\n\r\nsubtract 1\r\n\r\napply 10\r\n\r\n")]
        [TestCase("add 2\r\n\r\nsubtract 1\r\n\r\nlllyy 10\r\n\r\n")]
        public void FullTest_WrongCommand_ThrowException(string fileContents)
        {
            MemoryStream memoryStream = (MemoryStream)LastNoEmptyLineReader_Tests.CreateStreamFromString(fileContents);

            NoEmptyTrimLineReader textReader = new NoEmptyTrimLineReader(memoryStream);

            LastNoEmptyLineReader reader = new LastNoEmptyLineReader(memoryStream);
            string lastLine = reader.GetLastLine();

            CalcLineFactory calcFactory = new CalcLineFactory();
            calcFactory.Line = lastLine;
            calcFactory.CurrentCalc = null;

            Exception ex = Assert.Throws<Exception>(() => {
                calcFactory.CreateCalcFromLine();
                string line = textReader.ReadLine();
                while (line != null)
                {
                    calcFactory.Line = line;

                    calcFactory.CreateCalcFromLine();
                    line = textReader.ReadLine();
                }

            });

            StringAssert.Contains("Wrong command", ex?.Message);

        }

    }
}