﻿using FileCalc.Calc;
using FileCalc.Calc.Interfaces;
using NUnit.Framework;
using System;

namespace FileCalc.Tests.Unit
{
    [TestFixture]
    public class CalcFactory_Tests
    {
        private CalcLineFactory sut = null;
        private ICalc constCalc = null;
        private static int startConstValue = 21;

        [SetUp]
        public void Init()
        {
            sut = new CalcLineFactory();
            constCalc = new ApplyNumberCalc(startConstValue);
        }

        [Test]
        public void CreateCalcFromLine_LineWitWrongCommand_ThrowException()
        {
            ICalc calc = null;

            sut.Line = "nonamedCommand";
            sut.CurrentCalc = constCalc;

            Exception ex = Assert.Throws<Exception>(() =>
            {
                calc = sut.CreateCalcFromLine();

            });

            StringAssert.Contains("Wrong command", ex.Message);
        }




        [Test]
        public void CreateCalcFromLine_ApplyLine_ReturnConstCalcInstanceAndOkNumber()
        {
            Decimal expectedCalcValue = constCalc.Calculate();
            sut.Line = "apply " + expectedCalcValue.ToString();
            sut.CurrentCalc = constCalc;
            ICalc calc = sut.CreateCalcFromLine();

            Assert.IsInstanceOf<ApplyNumberCalc>(calc);
            Assert.AreEqual(expectedCalcValue, calc.Calculate());
        }

        [Test]
        public void CreateCalcFromLine_ApplyLineWithMinusNumber_ReturnConstCalcInstanceAndOkNumber()
        {
            Decimal expectedCalcValue = -19;
            sut.Line = "apply " + expectedCalcValue.ToString();
            sut.CurrentCalc = null;
            ICalc calc = sut.CreateCalcFromLine();

            Assert.IsInstanceOf<ApplyNumberCalc>(calc);
            Assert.AreEqual(expectedCalcValue, calc.Calculate());
        }

        [Test]
        public void CreateCalcFromLine_addLine_ReturnAddCalcInstance()
        {
            Decimal addValue = 10;
            Decimal expectedCalcValue = constCalc.Calculate() + addValue;
            sut.Line = "add " + addValue.ToString();
            sut.CurrentCalc = constCalc;

            ICalc calc = sut.CreateCalcFromLine();

            Assert.IsInstanceOf<AddCalc>(calc);
            Assert.AreEqual(expectedCalcValue, calc.Calculate());
        }

        [Test]
        public void CreateCalcFromLine_subtractLine_ReturnSubstractCalcInstance()
        {
            Decimal subValue = 10;
            Decimal expectedCalcValue = constCalc.Calculate() - subValue;
            sut.Line = "subtract " + subValue.ToString();
            sut.CurrentCalc = constCalc;

            ICalc calc = sut.CreateCalcFromLine();

            Assert.IsInstanceOf<SubtractCalc>(calc);
            Assert.AreEqual(expectedCalcValue, calc.Calculate());
        }

        [Test]
        public void CreateCalcFromLine_multiplyLine_ReturnMultiplyCalcInstance()
        {
            Decimal mulValue = 10;
            Decimal expectedCalcValue = constCalc.Calculate() * mulValue;
            sut.Line = "multiply " + mulValue.ToString();
            sut.CurrentCalc = constCalc;

            ICalc calc = sut.CreateCalcFromLine();

            Assert.IsInstanceOf<MultiplyCalc>(calc);
            Assert.AreEqual(expectedCalcValue, calc.Calculate());
        }

        [Test]
        public void CreateCalcFromLine_divideLine_ReturnDivideCalcInstance()
        {
            Decimal divValue = -3;
            Decimal expectedCalcValue = constCalc.Calculate() / divValue;
            sut.Line = "divide " + divValue.ToString();
            sut.CurrentCalc = constCalc;

            ICalc calc = sut.CreateCalcFromLine();

            Assert.IsInstanceOf<DivideCalc>(calc);
            Assert.AreEqual(expectedCalcValue, calc.Calculate());
        }

    }
}
