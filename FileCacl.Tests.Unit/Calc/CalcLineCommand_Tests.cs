﻿using System;
using System.Collections.Generic;
using System.Text;
using FileCalc.Calc;
using FileCalc.Calc.Interfaces;
using NUnit.Framework;

namespace FileCalc.Tests.Unit.Calc
{
    [TestFixture]
    public class CalcLineCommand_Tests
    {
        private CalcLineCommand sut = null;


        [SetUp]
        public void Init()
        {
            
        }


        #region Unit tests

        [Test]
        public void GetScore_NullCommand_ThrowExcpetion()
        {
            CreateSUT("null", "null");
            sut.Command = null;

            Exception ex = Assert.Throws<ArgumentNullException>(() =>
            {
                sut.GetScore();
            });

            StringAssert.Contains("Command is empty", ex.Message);
        }

        [Test]
        public void GetScore_EmptyCommand_ThrowExcpetion()
        {
            CreateSUT("null", "null");
            sut.Command = String.Empty;

            Exception ex = Assert.Throws<ArgumentNullException>(() =>
            {
                sut.GetScore();
            });

            StringAssert.Contains("Command is empty", ex.Message);
        }

        [Test]
        public void GetScore_EmptyValue_ThrowExcpetion()
        {
            CreateSUT("null", "null");
            sut.Command = "nullCommand";
            sut.Value = String.Empty;

            Exception ex = Assert.Throws<ArgumentNullException>(() =>
            {
                sut.GetScore();
            });

            StringAssert.Contains("Command Value is empty", ex.Message);
        }


        [Test]
        public void GetScore_CommandNotRecognized_ThrowExcpetion()
        {
            CreateSUT("null", "null");
            sut.Command = "nullCommand";
            sut.Value = "nullValue";

            Exception ex = Assert.Throws<Exception>(() =>
            {
                sut.GetScore();
            });

            StringAssert.Contains("Command not recognized", ex.Message);
        }


        [Test]
        public void GetScore_byDefault_ICaclObject()
        {
            CreateSUT("add", "2");

            object result = sut.GetScore();

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<ICalc>(result);
        }

        [TestCase("add","15", typeof(AddCalc))]
        [TestCase("subtract", "5", typeof(SubtractCalc))]
        [TestCase("apply", "10", typeof(ApplyNumberCalc))]
        [TestCase("multiply", "10", typeof(MultiplyCalc))]
        [TestCase("divide", "2", typeof(DivideCalc))]
        public void GetScore_CommandValue_ReturnOkICaclObject(string command, string value, Type exceptedType)
        {
            CreateSUT(command, value);

            object result = sut.GetScore();

            Assert.IsNotNull(result);
            Assert.IsInstanceOf<ICalc>(result);
            Assert.AreEqual(exceptedType, result.GetType());
        }

        [TestCase("apply", "aaaa")]
        [TestCase("add", "5+1")]
        [TestCase("multiply", "a10")]
        [TestCase("divide", "2b")]
        public void GetScore_CommandValueWrong_ThrowException(string command, string value)
        {
            CreateSUT(command, value);

            object result = null;

            Exception ex = Assert.Throws<Exception>(() =>
            {
                result = sut.GetScore();
            });

            StringAssert.Contains("Wrong number format for command ["+command+"] with value ["+value+"]", ex.Message);
        }


        #endregion

        #region Help function

        private CalcLineCommand CreateSUT(ICalc prevCalc, string command, string value)
        {
            sut = new CalcLineCommand(prevCalc, command, value);
            return sut;
        }
        private CalcLineCommand CreateSUT(string command, string value) => CreateSUT(null, command, value);
        private CalcLineCommand CreateSUT(string command) => CreateSUT(null, command, null);
        private CalcLineCommand CreateSUT() => CreateSUT(null, null, null);



        #endregion

    }
}
