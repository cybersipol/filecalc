﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FileCalc.IO;
using NUnit.Framework;


namespace FileCalc.Tests.Unit.IO
{
    [TestFixture]
    public class NoEmptyTrimLineReader_Tests
    {
        private NoEmptyTrimLineReader sut = null;

        [Test]
        public void ReadLine_byDefault_NoEmptyLines()
        {
            String str = "aaa" + Environment.NewLine + Environment.NewLine + "      " + Environment.NewLine + " bbb " + Environment.NewLine + Environment.NewLine;


            Stream stream = LastNoEmptyLineReader_Tests.CreateStreamFromString(str);
            CreateSUT(  stream );


            string line1 = sut.ReadLine();
            string line2 = sut.ReadLine();
            string line3 = sut.ReadLine();

            Assert.AreEqual("aaa", line1);
            Assert.AreEqual("bbb", line2);
            Assert.IsNull(line3);

        }

        [Test]
        public void ctor_StreamIsNull_throwExcpetion()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                CreateSUT(null);
            });
        }

        private NoEmptyTrimLineReader CreateSUT(Stream stream)
        {
            sut = new NoEmptyTrimLineReader(stream);
            return sut;
        }


    }
}
