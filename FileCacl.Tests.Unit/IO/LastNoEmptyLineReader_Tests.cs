using NUnit.Framework;
using FileCalc.IO;
using System.IO;
using System;
using System.Text;

namespace FileCalc.Tests.Unit.IO
{
    [TestFixture]
    public class LastNoEmptyLineReader_Tests
    {
        private LastNoEmptyLineReader sut = null;
        private Stream sutStream = null;
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void LastLine_StreamIsNull_ThrowException()
        {
            Stream stream = null;
            Assert.Throws<ArgumentNullException>(() => { sut = new LastNoEmptyLineReader(stream); });
        }

        [Test]
        public void LastLine_byDefault_LastLineWithApply()
        {
            CreateSUT("substract 3" + Environment.NewLine + "add 5" + Environment.NewLine + "apply 10");
            string lastLine = sut.GetLastLine();

            Assert.False(string.IsNullOrWhiteSpace(lastLine));
            Assert.AreEqual("apply 10",lastLine);
        }

        [Test]
        public void LastLine_byDefault_LastTrimLineWithApply()
        {
            CreateSUT("substract 3" + Environment.NewLine + "add 5" + Environment.NewLine + "    apply 10    ");
            string lastLine = sut.GetLastLine();

            Assert.False(string.IsNullOrWhiteSpace(lastLine));
            Assert.AreEqual("apply 10", lastLine);
        }

        [Test]
        public void LastLine_LastLineEmpty_LastLineWithApply()
        {
            CreateSUT("substract 3" + Environment.NewLine + "add 5" + Environment.NewLine + "apply 10" + Environment.NewLine + Environment.NewLine);
            string lastLine = sut.GetLastLine();

            Assert.False(string.IsNullOrWhiteSpace(lastLine));
            Assert.AreEqual("apply 10", lastLine);
        }

        [Test]
        public void LastLine_checkSeekStreamPosition_BeforeReadLastLinePositionInStream()
        {
            CreateSUT("substract 3" + Environment.NewLine + "add 5" + Environment.NewLine + "apply 10" + Environment.NewLine + Environment.NewLine);

            sutStream.ReadByte();
            sutStream.ReadByte();
            sutStream.ReadByte();

            long streamPos = sutStream.Position;

            string lastLine = sut.GetLastLine();

            Assert.AreEqual(streamPos, sutStream.Position);
        }

        private LastNoEmptyLineReader CreateSUT(Stream stream)
        {
            sutStream = stream;
            sut = new LastNoEmptyLineReader(stream);
            return sut;
        }

        private LastNoEmptyLineReader CreateSUT(String fileContent, Encoding encoding)
        {
            return CreateSUT(CreateStreamFromString(fileContent, encoding));
        }

        private LastNoEmptyLineReader CreateSUT(String fileContent)
        {
            return CreateSUT(fileContent, Encoding.UTF8);
        }

        internal static Stream CreateStreamFromString(String str, Encoding encoding)
        {
            if (str==null) return null;
            if (encoding == null) encoding = Encoding.UTF8;

            MemoryStream mem = new MemoryStream(encoding.GetBytes(str));
            
            return mem;
        }

        internal static Stream CreateStreamFromString(String str) => CreateStreamFromString(str, Encoding.UTF8);
    }
}