﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace FileCalc.IO
{
    public class NoEmptyTrimLineReader : StreamReader
    {
        public NoEmptyTrimLineReader(Stream stream) : this(stream, Encoding.UTF8)
        {
        }

        public NoEmptyTrimLineReader(Stream stream, Encoding encoding) : base(stream, encoding)
        {
        }

        public NoEmptyTrimLineReader(string path, Encoding encoding) : base(path, encoding)
        {
        }


        public override string ReadLine()
        {
            string line = base.ReadLine();
            while (line!=null)
            {
                if (!String.IsNullOrEmpty(line?.Trim())) return line.Trim();
                line = base.ReadLine();
            }

            return line;
        }
    }
}
