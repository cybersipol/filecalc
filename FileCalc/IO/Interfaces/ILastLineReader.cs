﻿using System.IO;

namespace FileCalc.IO.Interfaces
{
    public interface ILastLineReader
    {
        string GetLastLine();
    }
}