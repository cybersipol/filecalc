﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using FileCalc.IO.Interfaces;

namespace FileCalc.IO
{
    public class LastNoEmptyLineReader : ILastLineReader
    {
        public StreamReader StreamReader { get; private set; } = null;
        private long StreamPosition { get; set; } = 0;

        public LastNoEmptyLineReader(Stream stream)
        {
            if (stream == null) throw new ArgumentNullException(nameof(stream));
            StreamReader = new StreamReader(stream);
        }

        public LastNoEmptyLineReader(string filePath)
        {
            if (String.IsNullOrEmpty(filePath)) throw new ArgumentNullException(nameof(filePath));
            StreamReader = new StreamReader(new BufferedStream( new FileStream(filePath, FileMode.Open)));
        }

        public string GetLastLine()
        {
            if (StreamReader == null) throw new ArgumentNullException(nameof(StreamReader));
            SaveStreamPosition();

            string lastLine = null;
            string line = ReadTrimLine();
            while (line != null)
            {
                if (!String.IsNullOrEmpty(line))
                    lastLine = line;

                line = ReadTrimLine();
            }

            LoadStreamPosition();

            return lastLine;
        }

        private void SaveStreamPosition()
        {
            StreamPosition = StreamReader.BaseStream.Position;
        }

        private void LoadStreamPosition()
        {
            StreamReader.BaseStream.Seek(StreamPosition, SeekOrigin.Begin);
            StreamReader.BaseStream.Position = StreamPosition;
        }

        private string ReadTrimLine()
        {
            string line = StreamReader.ReadLine();

            return line?.Trim();
        }

    }
}
