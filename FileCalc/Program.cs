﻿using System;
using FileCalc.IO;
using System.IO;
using FileCalc.Calc;
using FileCalc.Calc.Interfaces;
using System.Text;

namespace FileCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string FilePath = GetFilePathFromArgs(args);

                // Configure Default list of commands
                CalcCommandConfigurator.Instance.ConfigureCommandList();

                FileCalcClient client = new FileCalcClient(FilePath, Encoding.UTF8);

                CalcLineFactory calcLineFactory = new CalcLineFactory();
                // Show file lines on console
                calcLineFactory.LineWriter = Console.Out;

                client.CalcLineFactory = calcLineFactory;
                client.LastLineReader = new LastNoEmptyLineReader(client.FileStream);

                Decimal score = client.CalcFile();
                Console.WriteLine("------------------------------------");
                Console.WriteLine("Score: {0}", score);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }

        /// <summary>
        /// Read FilePath from program arguments
        /// </summary>
        /// <param name="args">program arguments</param>
        /// <returns>FilePath from program arguments</returns>
        private static string GetFilePathFromArgs(string[] args)
        {
            if (args.Length <= 0)
                throw new Exception("No filepath argument");

            String FilePath = args[0];
            if (!IsFileExists(FilePath))
                throw new Exception(String.Format("File {0} not exists", FilePath));

            if (IsFileEmpty(FilePath))
                throw new Exception(String.Format("File {0} is empty", FilePath));

            return FilePath;
        }

        private static bool IsFileExists(string FilePath)
        {
            FileInfo FilePathInfo = new FileInfo(FilePath);
            FilePathInfo.Refresh();
            return FilePathInfo.Exists;
        }

        private static bool IsFileEmpty(string FilePath)
        {
            FileInfo FilePathInfo = new FileInfo(FilePath);
            FilePathInfo.Refresh();
            return (FilePathInfo.Length <= 0);
        }
    }
}
