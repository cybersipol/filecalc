﻿using FileCalc.Calc;
using FileCalc.Calc.Interfaces;
using FileCalc.IO;
using FileCalc.IO.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileCalc
{
    public class FileCalcClient
    {
        public Stream FileStream { get; protected set; } = null;
        private Encoding encoding = Encoding.UTF8;

        public ILastLineReader LastLineReader { get; set; } = null;
        public ICalcFactory CalcLineFactory { get; set; } = null;



        #region Constructors
        public FileCalcClient(string path, Encoding encoding)
        {
            if (String.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            this.encoding = encoding;

            FileInfo pathInfo = new FileInfo(path);
            pathInfo.Refresh();
            if (!pathInfo.Exists) throw new FileNotFoundException("File [" + path ?? "(null)" + "] not exists");
            
            FileStream = new FileStream(path, FileMode.Open);
            
        }

        public FileCalcClient(string path): this(path, Encoding.UTF8)
        {
        }


        public FileCalcClient(Stream fileStream): this(fileStream, Encoding.UTF8)
        {
        }

        public FileCalcClient(Stream fileStream, Encoding encoding)
        {
            this.FileStream = fileStream ?? throw new ArgumentNullException(nameof(fileStream));
            this.encoding = encoding;
        }
        #endregion


        public Decimal CalcFile()
        {
            CheckIsInstalledInterfaces();

            SeekFileStreamToBegin();

            LastLineRead();

            StreamReader streamReader = new NoEmptyTrimLineReader(FileStream);
            string line = streamReader.ReadLine();
            while (line != null)
            {
                CalcLineFactory.Line = line;
                CalcLineFactory.CreateCalcFromLine();

                line = streamReader.ReadLine();
            }

            return CalcLineFactory.CurrentCalc.Calculate();
        }


        #region Help function

        private void LastLineRead()
        {
            CalcLineFactory.Line = LastLineReader.GetLastLine();
            if (String.IsNullOrEmpty(CalcLineFactory.Line)) throw new Exception("File is empty");
            CalcLineFactory.CreateCalcFromLine();

            if (CalcLineFactory.CurrentCalc == null) throw new Exception("Cannot localize apply line");
            if (!CalcCommandConfigurator.Instance.IsApplyType( CalcLineFactory.CurrentCalc ) ) throw new Exception("Cannot localize apply line");
        }

        private void CheckIsInstalledInterfaces()
        {
            CheckIsInstalledLastLineReader();
            CheckIsInstalledCaclLineFactory();
        }

        private void CheckIsInstalledLastLineReader()
        {
            if (LastLineReader == null) throw new ArgumentNullException("No installed LastLineReader");
        }

        private void CheckIsInstalledCaclLineFactory()
        {
            if (CalcLineFactory == null) throw new ArgumentNullException("No installed CalcLineFactory");
        }

        private void SeekFileStreamToBegin()
        {
            if (FileStream == null) throw new ArgumentNullException("FileStream is null");
            
            FileStream.Seek(0, SeekOrigin.Begin);
            FileStream.Position = 0;

        }


        #endregion

    }
}
