﻿using FileCalc.Calc.Interfaces;
using System;
using System.IO;
using System.Text.RegularExpressions;

namespace FileCalc.Calc
{
    public class CalcLineFactory : ICalcFactory
    {
        public String Line { get; set; } = String.Empty;
        public ICalc CurrentCalc { get; set; } = null;
        public TextWriter LineWriter { get; set; } = null;

        private ICalc CreateCalcFromLine(string line, ICalc prevoiusCalc = null)
        {
            CalcCommandLineRecognizer commandLineRecognizer = new CalcCommandLineRecognizer(CalcCommandConfigurator.Instance.GetCommandList(), prevoiusCalc);
            commandLineRecognizer.Line = line;
            ILineCommand<ICalc> lineCommand = commandLineRecognizer.LineRecognition();

            ICalc ret = lineCommand.GetScore();
            if (ret is CalcNull) return prevoiusCalc;

            if (CalcCommandConfigurator.Instance.IsApplyType(ret))
                return (prevoiusCalc ?? ret);

            return ret;
        }

        public ICalc CreateCalcFromLine()
        {
            if (LineWriter != null && CurrentCalc!=null) LineWriter.WriteLine(Line);

            CurrentCalc = CreateCalcFromLine(Line, CurrentCalc);
            
            return CurrentCalc;
        }
    }
}
