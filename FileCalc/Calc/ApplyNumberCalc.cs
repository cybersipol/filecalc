﻿using FileCalc.Calc.Interfaces;
using System;

namespace FileCalc.Calc
{
    public class ApplyNumberCalc: Calc
    {
        public ApplyNumberCalc(ICalc cacl, Decimal param) : base(param)
        {
        }

        public ApplyNumberCalc(Decimal param): base(param)
        {
        }

        public override Decimal Calculate()
        {
            return Param;
        }
    }
}
