﻿using FileCalc.Calc.Interfaces;
using System;

namespace FileCalc.Calc
{
    public class SubtractCalc : Calc
    {

        public SubtractCalc(ICalc calc, Decimal param): base(calc, param)
        {
        }

        public override Decimal Calculate()
        {
            return calc.Calculate() - Param;
        }
    }
}
