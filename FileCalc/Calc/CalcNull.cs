﻿using System;
using System.Collections.Generic;
using System.Text;
using FileCalc.Calc.Interfaces;

namespace FileCalc.Calc
{
    public class CalcNull : Calc
    {
        public CalcNull(): base(0)
        {
            calc = this;
            Param = 0;
        }

        public CalcNull(decimal param) : this()
        {
        }

        public CalcNull(ICalc calc, decimal param) : this()
        {
        }

        public override decimal Calculate()
        {
            throw new Exception("Null Calculate");
        }
    }
}
