﻿using FileCalc.Calc.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FileCalc.Calc
{
    public class CalcLineCommand : ILineCommand<ICalc>
    {
        #region Constructors

        public CalcLineCommand(ICalc prevCalc)
        {
            PrevCalc = prevCalc;
        }

        public CalcLineCommand(ICalc prevCalc, string command, string value)
        {
            PrevCalc = prevCalc;
            Command = command ?? throw new ArgumentNullException(nameof(command));
            Value = value ?? String.Empty;
        }

        public CalcLineCommand(ICalc prevCalc, string command) : this(prevCalc, command, String.Empty)
        {
        }


        #endregion

        #region Variables 

        public ICalc PrevCalc { get; private set; } = null;

        public String Command { get; set; } = String.Empty;
        public String Value { get; set; } = String.Empty;

        #endregion

        public ICalc GetScore()
        {
            if (String.IsNullOrEmpty(Command?.Trim()))
                throw new ArgumentNullException("Command is empty");

            if (String.IsNullOrEmpty(Value?.Trim()))
                throw new ArgumentNullException("Command Value is empty");

            Type scoreType = CalcCommandConfigurator.Instance.GetTypeForCommand(Command);
            if (scoreType == null) throw new Exception("Command not recognized [" + Command + "]");

            try
            {
                Decimal val = Decimal.Parse(Value?.Trim());
                return ((ICalc)Activator.CreateInstance(scoreType, new object[] { PrevCalc, val }));
            }
            catch (Exception ex)
            {
                throw new Exception("Wrong number format for command [" + Command + "] with value [" + Value + "]", ex);
            }

            return Calc.Null;
        }
    }
}
