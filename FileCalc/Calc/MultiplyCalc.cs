﻿using FileCalc.Calc.Interfaces;
using System;

namespace FileCalc.Calc
{
    public class MultiplyCalc : Calc
    {

        public MultiplyCalc(ICalc calc, Decimal param): base(calc, param)
        {
        }

        public override Decimal Calculate()
        {
            return calc.Calculate() * Param;
        }
    }
}
