﻿using FileCalc.Calc.Interfaces;
using System;

namespace FileCalc.Calc
{
    public class AddCalc : Calc
    {
        public AddCalc(ICalc calc, Decimal param) : base(calc, param)
        {
        }

        public override Decimal Calculate()
        {
            return calc.Calculate() + Param;
        }
    }
}
