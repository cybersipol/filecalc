﻿using System.Collections.Generic;
using FileCalc.Calc.Interfaces;

namespace FileCalc.Calc.Interfaces
{
    public interface ICommandLineRecognizer<T>
    {
        IEnumerable<string> CommandsList { get; }
        string Line { get; set; }

        ILineCommand<T> LineRecognition();
    }
}