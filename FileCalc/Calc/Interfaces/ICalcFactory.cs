﻿using FileCalc.Calc.Interfaces;
using System;

namespace FileCalc.Calc.Interfaces
{
    public interface ICalcFactory
    {
        String Line { get; set; }
        ICalc CurrentCalc { get; set; }

        ICalc CreateCalcFromLine();
    }
}