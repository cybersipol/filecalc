﻿using System;

namespace FileCalc.Calc.Interfaces
{
    public interface ICalc
    {
        Decimal Param { get; }
        Decimal Calculate();
    }
}
