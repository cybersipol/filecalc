﻿namespace FileCalc.Calc.Interfaces
{
    public interface ILineCommand<T>
    {
        string Command { get; set; }
        string Value { get; set; }

        T GetScore();
    }
}