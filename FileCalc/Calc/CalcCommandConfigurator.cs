﻿using FileCalc.Calc.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FileCalc.Calc
{
    internal sealed class CalcCommandConfigurator
    {
        private static CalcCommandConfigurator _instance = null;
        private static string ApplyCommand = "apply";
        private static Type ApplyType = typeof(ApplyNumberCalc);

        private CalcCommandConfigurator()
        {
            AssociationsCommandsAndTypes = new Dictionary<string, Type>();
        }

        private Dictionary<string, Type> AssociationsCommandsAndTypes { get; set; } = null;

        public static CalcCommandConfigurator Instance => getInstance();

        public void ConfigureCommandList()
        {
            if (AssociationsCommandsAndTypes.Count > 0) return;

            AssociationsCommandsAndTypes.Add("add", typeof(AddCalc));
            AssociationsCommandsAndTypes.Add("subtract", typeof(SubtractCalc));
            AssociationsCommandsAndTypes.Add("multiply", typeof(MultiplyCalc));
            AssociationsCommandsAndTypes.Add("divide", typeof(DivideCalc));

            AddApplyAssociation();
        }
        
        private void AddApplyAssociation()
        {
            if (AssociationsCommandsAndTypes.ContainsKey(ApplyCommand)) return;

            AssociationsCommandsAndTypes.Add(ApplyCommand, ApplyType);
        }

        public Type GetTypeForCommand(string command)
        {
            command = command?.Trim().ToLower();
            
            if (AssociationsCommandsAndTypes.Count <= 0) ConfigureCommandList();

            if (AssociationsCommandsAndTypes.ContainsKey(command))
                return AssociationsCommandsAndTypes[command];

            return null;
        }

        public bool IsApplyCommand(string command) => ApplyCommand.Equals(command);

        public bool IsApplyType(ICalc calc) => calc.GetType() == ApplyType;

        public IEnumerable<string> GetCommandList()
        {
            ConfigureCommandList();
            return AssociationsCommandsAndTypes.Keys;
        }


        private static CalcCommandConfigurator getInstance()
        {
            if (_instance == null)
                _instance = new CalcCommandConfigurator();
            return _instance;
        }

    }
}
