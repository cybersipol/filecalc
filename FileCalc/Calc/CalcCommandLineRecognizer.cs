﻿using FileCalc.Calc.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FileCalc.Calc
{
    public class CalcCommandLineRecognizer : ICommandLineRecognizer<ICalc>
    {
        public CalcCommandLineRecognizer(IEnumerable<string> commandsList, ICalc prevCalc)
        {
            CommandsList = commandsList ?? throw new ArgumentNullException(nameof(commandsList));
            PrevCalc = prevCalc;
        }

        public ICalc PrevCalc { get; private set; } = null;

        public String Line { get; set; } = null;
        public IEnumerable<string> CommandsList { get; private set; } = null;

        public ILineCommand<ICalc> LineRecognition()
        {
            Match m = Regex.Match(Line, "^(" + String.Join('|', CommandsList) + ")([ ]{1})(.+)$");
            if (!m.Success) throw new Exception("Wrong command");

            string command = m.Groups[1].Value;
            string stringValue = m.Groups[3].Value;

            return new CalcLineCommand(PrevCalc, command, stringValue);
        }
    }
}
