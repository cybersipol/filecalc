﻿using FileCalc.Calc.Interfaces;
using System;

namespace FileCalc.Calc
{
    public class DivideCalc : Calc
    {

        public DivideCalc(ICalc calc, Decimal param): base(calc, param)
        {
        }

        public override Decimal Calculate()
        {
            return (calc.Calculate() / Param);
        }
    }
}
