﻿using FileCalc.Calc.Interfaces;
using System;

namespace FileCalc.Calc
{
    public abstract class Calc : ICalc
    {
        private ICalc _calc = null;

        public static ICalc Null { get; } = new CalcNull();

        public Decimal Param { get; protected set; } = 0;
        protected ICalc calc { get => (_calc ?? Null); set => _calc = value; }

        protected Calc(ICalc calc, Decimal param)
        {
            this.calc = calc ?? Null;
            this.Param = param;
        }

        protected Calc(Decimal param)
        {
            this.calc = null;
            this.Param = param;
        }

        public virtual Decimal Calculate()
        {
            return ((_calc != null && !(_calc is CalcNull)) ? _calc.Calculate() : Decimal.Zero);
        }

    }
}
